var arr = [];
document.getElementById("btnAdd").onclick = function(){
    var arrNumber = document.querySelector("#txt-number");
    if(arrNumber){
        arr.push(arrNumber.value*1);
    }
    console.log(arr);
    arrNumber.value = '';
    document.getElementById("ArrayNumber").innerHTML = `
        <h3>Các số có trong mảng: ${arr}</h3>
    `;
}
// BT1: Tổng số dương
document.getElementById("btn1").ondblclick = function(){
   var item1 = document.getElementById("item1");
   if (item1.style.display === "none") {
    item1.style.display = "block";
  } else {
    item1.style.display = "none";
  }
}
document.getElementById("exer1").onclick = function(){
    var sumBt1 = 0;
    for(var i = 0; i < arr.length; i++){
        if (arr[i] > 0) {
            sumBt1 += arr[i];
        }
    }
    document.getElementById("result-bt1").innerHTML = `
        <h3>Tổng số dương: ${sumBt1}</h3>
    `;
}
// BT2: Đếm số dương
document.getElementById("btn2").ondblclick = function(){
   var item2 = document.getElementById("item2");
   if (item2.style.display === "none") {
    item2.style.display = "block";
  } else {
    item2.style.display = "none";
  }
}
document.getElementById("exer2").onclick = function(){
    var countBT2 = 0;
    for(var i = 0; i < arr.length; i++){
        if (arr[i] > 0) {
            countBT2++;
        }
    }
    document.getElementById("result-bt2").innerHTML = `
        <h3>Số dương: ${countBT2}</h3>
    `;
}
// BT3: Tìm số nhỏ nhất
document.getElementById("btn3").ondblclick = function(){
   var item3 = document.getElementById("item3");
   if (item3.style.display === "none") {
    item3.style.display = "block";
  } else {
    item3.style.display = "none";
  }
}
document.getElementById("exer3").onclick = function(){
    var minNum= arr[0];
    for(var i = 0; i < arr.length; i++){
        if (minNum > arr[i]) {
            minNum = arr[i];
        }
    }
    document.getElementById("result-bt3").innerHTML = `
        <h3>Số nhỏ nhất: ${minNum}</h3>
    `;
}
// BT4 : Tìm số dương nhỏ nhất
document.getElementById("btn4").ondblclick = function(){
   var item4 = document.getElementById("item4");
   if (item4.style.display === "none") {
    item4.style.display = "block";
  } else {
    item4.style.display = "none";
  }
}

document.getElementById("exer4").onclick = function(){
  var listSoDuong = [];
  for(var i = 0; i < arr.length; i++){
    if (arr[i] > 0) {
      listSoDuong.push(arr[i]);
    }
  }console.log(listSoDuong);
  if (listSoDuong.length > 0) {
      var smallestNumber = Math.min.apply(Math, listSoDuong);
      document.getElementById("result-bt4").innerHTML = `<h3>Số dương nhỏ nhất: ${smallestNumber}</h3>`;
    } else {
      document.getElementById("result-bt4").innerHTML = `<h3>Not found</h3>`;
    }
}
// BT5: Tìm số chẵn cuối cùng
document.getElementById("btn5").ondblclick = function(){
   var item5 = document.getElementById("item5");
   if (item5.style.display === "none") {
    item5.style.display = "block";
  } else {
    item5.style.display = "none";
  }
}
document.getElementById("exer5").onclick = function(){
  var soChanCuoiCung = 0;
  var n = arr.length;
  for(var i = n - 1; i < n; i--){
    if(arr[i] % 2 == 0){// neu a[i] la so chan
      soChanCuoiCung = arr[i];
      break;
      // break;
    }   
  }
  document.getElementById("result-bt5").innerHTML = `
    <h3>Sỗ chẵn cuối cùng: ${soChanCuoiCung}</h3>
  `;
}
// BT6: Đổi chỗ
document.getElementById("btn6").ondblclick = function(){
   var item6 = document.getElementById("item6");
   if (item6.style.display === "none") {
    item6.style.display = "block";
  } else {
    item6.style.display = "none";
  }
}
document.getElementById("exer6").onclick = function(){
  var position1 = Number(document.getElementById("position1").value);
  var position2 = Number(document.getElementById("position2").value);
  var value1 = arr[position1];
  var value2 = arr[position2];
  arr[position1] = value2;
  arr[position2] = value1;
  document.getElementById("result-bt6").innerHTML = `
    <h3>Mảng sau khi đổi: ${arr}</h3>
  `;
}
// BT7: Sắp xếp tăng dần
document.getElementById("btn7").ondblclick = function(){
   var item7 = document.getElementById("item7");
   if (item7.style.display === "none") {
    item7.style.display = "block";
  } else {
    item7.style.display = "none";
  }
}
document.getElementById("exer7").onclick = function(){
  const compareFn = (a, b) => (a < b ? -1 : 0);
  arr.sort(compareFn);
  console.log("Mảng đã sắp xếp:",arr);
  document.getElementById("result-bt7").innerHTML = `
    <h3>Mảng đã sắp xếp: ${arr}</h3>
  `;
}
// BT8: Tìm số nguyên tố đầu tiên trong mảng
document.getElementById("btn8").ondblclick = function(){
   var item8 = document.getElementById("item8");
   if (item8.style.display === "none") {
    item8.style.display = "block";
  } else {
    item8.style.display = "none";
  }
}
function isPrime(num) {
  for (let i = 2; i < num; i++) {
    if (num % i === 0) {
      return false;
    }
  }
  return true;
}
document.getElementById("exer8").onclick = function(){
  var firstPrimeNumber = arr[i];
  var ketquaBt8 = '<h3>-1</h3>';
  for (var i = 1; i < arr.length; i++) {
  // Check if the number is prime
    if (isPrime(arr[i])){
    // Set the first prime number to the current number
      firstPrimeNumber = arr[i];
      ketquaBt8 = `<h3>Số nguyên tố đầu tiên trong mảng: ${firstPrimeNumber}</h3>`;
      break;
    }
  }
  console.log(firstPrimeNumber);
  document.getElementById("result-bt8").innerHTML = ketquaBt8;
}
// BT9: Nhập mảng mới và đếm số nguyên
document.getElementById("btn9").ondblclick = function(){
   var item9 = document.getElementById("item9");
   if (item9.style.display === "none") {
    item9.style.display = "block";
  } else {
    item9.style.display = "none";
  }
}
var arrNew = [];
document.getElementById("addNewArray").onclick = function(){
  var arrayNewNumber = document.querySelector("#newArray");
  if(arrayNewNumber){
    arrNew.push(arrayNewNumber.value*1);
  }
  document.getElementById("arrayNew").innerHTML = `
    <h3>Mảng mới được nhập: ${arrNew}</h3>  
  `;
}
document.getElementById("exer9").onclick = function(){
  var countBt9 = 0;
  var numberCountBt9 = '';
  for (var i = 0; i < arrNew.length; i++){
    if (Number.isInteger(arrNew[i])) {
      countBt9++;
      numberCountBt9 += `${arrNew[i]}, `;
    }
  }
  document.getElementById("result-bt9").innerHTML = `
    <h3>Số nguyên: ${countBt9}</h3>
    <h3>Các số nguyên: ${numberCountBt9}</h3>
  `;
}
// BT10
document.getElementById("btn10").ondblclick = function(){
   var item10 = document.getElementById("item10");
   if (item10.style.display === "none") {
    item10.style.display = "block";
  } else {
    item10.style.display = "none";
  }
}
document.getElementById("exer10").onclick = function(){
  var countDuong = 0;
  var countAm = 0;
  var contentBt10 = '';
  for(var i = 0; i < arr.length; i++){
    if (arr[i] > 0) {
      countDuong++;
    }else{
      countAm++;
    }
  }
  console.log("+:",countDuong);
  console.log("-:",countAm);
  if (countDuong > countAm) {
    contentBt10 = `<h3>Số lượng dương > số lượng âm</h3>`;
  }else if(countDuong < countAm){
    contentBt10 = `<h3>Số lượng dương < số lượng âm</h3>`;
  }else{
    contentBt10 = `<h3>Số lượng dương = số lượng âm</h3>`;
  }
  document.getElementById("result-bt10").innerHTML = contentBt10;
}

